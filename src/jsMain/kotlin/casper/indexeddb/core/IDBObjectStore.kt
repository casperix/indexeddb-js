package casper.indexeddb.core

external open class IDBObjectStore {
	val autoIncrement:Any? = definedExternally
	val indexNames:Any? = definedExternally
	val keyPath:Any? = definedExternally
	val name:Any? = definedExternally
	val transaction: IDBTransaction = definedExternally

	fun add(value:Any): IDBRequest
	fun add(value:Any, key:Any): IDBRequest
	fun clear(): IDBRequest
	fun count(): IDBRequest
	fun count(range: IDBKeyRange): IDBRequest
	fun createIndex(indexName:String?, keyPath:String?): IDBIndex
	fun createIndex(indexName:String?, keyPath:String?, objectParameters: IDBIndexParameters): IDBIndex
	fun delete(key:Any): IDBRequest
	fun deleteIndex(indexName:String)
	fun get(key:Any): IDBRequest
	fun getAll(key:Any): IDBRequest
	fun getAll(key:Any, query: IDBKeyRange): IDBRequest
	fun getAll(key:Any, query: IDBKeyRange, count:Int): IDBRequest
	fun getAllKeys(key:Any): IDBRequest
	fun getAllKeys(key:Any, query: IDBKeyRange): IDBRequest
	fun getAllKeys(key:Any, query: IDBKeyRange, count:Int): IDBRequest
	fun getKey(key:Any): IDBRequest
	fun index(indexName:String): IDBIndex

	fun openCursor()
	fun openKeyCursor()
	fun put(value:Any): IDBRequest
	fun put(value:Any, key:Any): IDBRequest
}