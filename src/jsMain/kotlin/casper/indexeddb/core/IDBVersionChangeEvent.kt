package casper.indexeddb.core

external open class IDBVersionChangeEvent {
	val oldVersion:Int = definedExternally
	val newVersion:Int = definedExternally
	val version:Int = definedExternally
}