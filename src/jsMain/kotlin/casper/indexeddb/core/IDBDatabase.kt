package casper.indexeddb.core

import org.w3c.dom.events.Event
import org.w3c.dom.events.EventTarget

class DBMode {
	companion object {
		val READONLY = "readonly"
		val READWRITE = "readwrite"
		val VERSIONCHANGE = "versionchange"
	//	val READWRITEFLUSH = "readwriteflush"
	}
}


class Options(val keyPath:String = "", val autoIncrement:Boolean = false)

external open class IDBDatabase : EventTarget {

	val name:Any
	val objectStoreNames:Array<String>
	val onabort:Any
	val onclose:Any
	val onerror:Any
	val onversionchange:Any
	val version:Int

	fun close()
	fun createObjectStore(name: String): IDBObjectStore
	fun createObjectStore(name: String, options: Options): IDBObjectStore
	fun deleteObjectStore(name: String)
	fun transaction(storeNames: Array<String>): IDBTransaction
	fun transaction(storeNames: Array<String>, mode: String): IDBTransaction
	fun transaction(storeName: String): IDBTransaction
	fun transaction(storeName: String, mode: String): IDBTransaction

	open var versionchange: ((Event) -> dynamic)?= definedExternally
}