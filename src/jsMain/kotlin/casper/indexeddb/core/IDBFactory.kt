package casper.indexeddb.core

import org.w3c.dom.Window
import org.w3c.dom.get
import kotlin.js.Promise

external class IDBFactory {
	fun open(name: String, version: Int? = definedExternally): IDBOpenDBRequest
	fun deleteDatabase(name:String): IDBOpenDBRequest
	fun databases(): Promise<Any>//??	Array<IDBDatabase>
	fun cmp(first: Any, second: Any): Int
}

val Window.indexedDB: IDBFactory?
	get() = window["indexedDB"] as? IDBFactory