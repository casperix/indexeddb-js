package casper.indexeddb.core

import casper.indexeddb.core.IDBObjectStore
import org.w3c.dom.events.Event
import org.w3c.dom.events.EventTarget

external open class IDBTransaction : EventTarget {
	var db: Any? = definedExternally
	var error: Any? = definedExternally
	var mode: Any? = definedExternally
	var objectStoreNames: Any? = definedExternally
	var onabort: Any? = definedExternally
	var oncomplete: ((Event) -> dynamic)? = definedExternally
	var onerror: ((Event) -> dynamic)? = definedExternally

	fun abort()
	fun objectStore(name:String): IDBObjectStore
	fun commit()

	var abort: ((Event) -> dynamic)?
	var complete: ((Event) -> dynamic)?


}