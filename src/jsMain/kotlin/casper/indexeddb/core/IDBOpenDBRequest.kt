package casper.indexeddb.core

import org.w3c.dom.events.Event
import org.w3c.dom.events.EventTarget

abstract external class IDBOpenDBRequest : IDBRequest, EventTarget {
	open var blocked: ((IDBVersionChangeEvent) -> dynamic)? = definedExternally
	open var upgradeneeded: ((Event) -> dynamic)?= definedExternally
	open var onupgradeneeded: ((IDBVersionChangeEvent) -> dynamic)?= definedExternally
}