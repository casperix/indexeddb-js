package casper.indexeddb.core

external open class IDBIndexParameters
external open class IDBCursor
external open class IDBCursorSync
external open class IDBCursorWithValue
external open class IDBDatabaseException
external open class IDBDatabaseSync
external open class IDBEnvironment
external open class IDBEnvironmentSync
external open class IDBFactorySync
external open class IDBIndex
external open class IDBIndexSync
external open class IDBKeyRange
external open class IDBObjectStoreSync
external open class IDBTransactionSync
external open class IDBVersionChangeRequest