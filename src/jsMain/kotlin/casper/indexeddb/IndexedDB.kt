package casper.indexeddb

import casper.indexeddb.core.DBMode
import casper.indexeddb.core.IDBDatabase
import casper.indexeddb.core.indexedDB
import kotlin.browser.window
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

suspend fun connect(bdName: String, storeList: () -> Set<String>): IDBDatabase = suspendCoroutine { future ->
	val indexedDB = window.indexedDB
	if (indexedDB != null) {
		val request = indexedDB.open(bdName, 1)
		request.onerror = {
			future.resumeWithException(Error("DB open failed $it"))
		}
		request.onsuccess = {
			val db = request.result
			if (db is IDBDatabase) {
				future.resume(db)
			}
		}
		request.onupgradeneeded = {
			val db = request.result
			if (db is IDBDatabase) {
				storeList().forEach { storeName ->
					db.createObjectStore(storeName)
				}
			}
		}
	} else {
		future.resumeWithException(Error("IndexedDB not supported"))
	}
}

suspend fun IDBDatabase.read(storeName: String, keyName: String): String = suspendCoroutine { future ->
	val transaction = this.transaction(storeName, DBMode.READONLY)
	transaction.onerror = {
		future.resumeWithException(Error("DB can't open for read"))
	}
	val objectStore = transaction.objectStore(storeName)
	val request = objectStore.get(keyName)
	request.onsuccess = {
		val res = request.result
		if (res is String) {
			future.resume(res)
		} else {
			future.resumeWithException(Error("Invalid output format: $res"))
		}
	}
	request.onerror = {
		future.resumeWithException(Error("DB read request failed"))
	}
}

suspend fun IDBDatabase.write(storeName: String, keyName: String, value: String): Unit = suspendCoroutine { future ->
	val transaction = this.transaction(storeName, DBMode.READWRITE)
	transaction.onerror = {
		future.resumeWithException(Error("DB can't open for write"))
	}

	val objectStore = transaction.objectStore(storeName)
	val request = objectStore.put(value, keyName)
	request.onsuccess = {
		future.resume(Unit)
	}
	request.onerror = {
		future.resumeWithException(Error("DB write request failed"))
	}
}

suspend fun IDBDatabase.clear(storeName: String, keyName: String): Unit = suspendCoroutine { future ->
	val transaction = this.transaction(storeName, DBMode.READWRITE)
	transaction.onerror = {
		future.resumeWithException(Error("DB can't open for clear"))
	}

	val objectStore = transaction.objectStore(storeName)
	val request = objectStore.delete(keyName)
	request.onsuccess = {
		future.resume(Unit)
	}
	request.onerror = {
		future.resumeWithException(Error("DB clear request failed"))
	}
}