package casper.util

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.promise

actual fun runSuspend(block: suspend () -> Unit): dynamic = GlobalScope.promise { block() }