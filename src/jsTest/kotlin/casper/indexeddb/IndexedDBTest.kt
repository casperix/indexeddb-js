package casper.indexeddb

import casper.util.runSuspend
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFails
import kotlin.test.assertTrue

class IndexedDBTest {
	@Test
	fun testConnect() = runSuspend {
		val db = connect("testBD") { setOf("testStore") }
		assertTrue { true }
		db.close()
	}

	@Test
	fun testReadWrite() = runSuspend {
		val db = connect("testBD") { setOf("testStore") }
		db.write("testStore", "testKey", "testValue")
		val value = db.read("testStore", "testKey")
		db.close()
		assertEquals("testValue", value)
	}

	@Test
	fun writeToInvalidStore() = runSuspend {
		val db = connect("testBD") { setOf("testStore") }
		assertFails {
			db.write("invalidStore", "testKey", "testValue")
		}
		db.close()
	}

	@Test
	fun readFromInvalidStore() = runSuspend {
		val db = connect("testBD") { setOf("testStore") }
		assertFails {
			db.read("invalidStore", "testKey")
		}
		db.close()
	}

	@Test
	fun readFromInvalidKey() = runSuspend {
		val db = connect("testBD") { setOf("testStore") }
		assertFails {
			db.read("testStore", "invalidKey")
		}
		db.close()
	}

	@Test
	fun testClear() = runSuspend {
		val db = connect("testBD") { setOf("testStore") }
		db.write("testStore", "testKey", "testValue")
		db.clear("testStore", "testKey")
		assertFails {
			db.read("testStore", "testKey")
		}
		db.close()
	}
}