package casper.util

expect fun runSuspend(block: suspend () -> Unit)